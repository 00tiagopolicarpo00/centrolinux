---
title: "Centro Linux"

description: "O espaço para a comunidade de GNU/Linux"
cascade:
  featured_image: ''
---

O Centro de Linux é uma iniciativa da Comunidade Ubuntu Portugal, para promover a partilha de conhecimento e experiências na comunidade de GNU/Linux, Software Livre e Open Source Software.

A iniciativa faculta um espaço físico (na verdade o espaço é do [Makers in Little Lisbon](https://mill.pt/)) e um laboratório (computadores), com os quais a comunidade pode realizar as suas actividades em conjunto.

A actividade do Centro Linux é dirigida pela Comunidade Ubuntu Portugal, e podem ser organizadas pela própria, ou em colaboração outras comunidades amigas e com voluntários individuais que se ofereçam para cooperar e organizar actividades individuais, ou um conjunto de actividades.

