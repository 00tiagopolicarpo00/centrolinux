---
title: "O Centro Linux"
description: "O que é o Centro Linux?"
featured_image: ''
menu:
  main:
    weight: 1
---

O Centro Linux é uma iniciativa que procura proporcionar experiências comunitárias práticas em um local físico. Workshops, hackatons, demonstrações, instalações, apoio técnico comunitário, etc... E como tal não só tem um laboratório onde os participantes podem de facto por "as mãos no teclado", como o tem em um dos melhores locais possíveis para uma comunidade deste tipo, um maker space.

O [MILL – Makers In Little Lisbon](https://mill.pt) é a casa do Centro Linux, e é localizado em Lisboa, perto do Campo dos Mártires da Pátria. Se querem saber como chegar, temos [uma pagina com essas informações de onde estamos localizados](https://centrolinux.pt/about/ondeestamos/).

O Centro Linux é uma iniciativa promovida e gerida pela Comunidade [Ubuntu-pt](https://www.ubuntu-pt.org), mas que não se encerra nesta comunidade e procura estar aberta a todas as outras comunidades de distribuições de GNU/Linux, de Software Livre e Open Source Software e até algumas das outras comunidades que promovem Cultura Aberta, Dados Abertos, etc...

Também importante para o que somos é o comportamento razoável que rege todos os que participam nesta iniciativa.

Estando nos em uma casa "emprestada", o MILL, é importante que todos se lembrem de seguir as regras do MILL, que procurem conservar o espaço a sua arrumação e limpeza e sigam as inidicações dos seus responsáveis.

Sendo uma iniciativa da Comunidade Ubuntu-pt, também temos a mesma [missão do Ubuntu](https://ubuntu.com/community/mission), seguimos o [Código de Conduta](https://ubuntu.com/community/code-of-conduct) do Ubuntu, e a [Declaração de Diversidade](https://ubuntu.com/community/mission). Acreditamos que estes valores são compatívies com os de qualquer outra das comunidades que a cima mencionamos como bem-vindas à nossa iniciativa.

#### Apoios:

[![Ubuntu-pt](https://centrolinux.pt/images/BLHCBzYG_400x400.jpg "Ubuntu-pt")](https://ubuntu-pt.org)
[![Localização do MILL](https://centrolinux.pt/images/web_header.png "Makers In Litle Lisbon")](https://mill.pt)
