---
title: "Onde estamos"
description: "Onde é o Centro Linux"
menu:
  main:
    weight: 1
---

O Centro Linux é uma iniciativa que procura proporcionar experiências comunitárias práticas em um local físico. Workshops, hackatons, demonstrações, instalações, apoio técnico comunitário, etc... E como tal não só tem um laboratório onde os participantes podem de facto por "as mãos no teclado", como o tem em um dos melhores locais possíveis para uma comunidade deste tipo, um maker space.

O [MILL – Makers In Little Lisbon](https://mill.pt) é a casa do Centro Linux, e é localizado em Lisboa, perto do Campo dos Mártires da Pátria.

#### Como chegar ao MILL/Centro Linux

MILL – Makers In Little Lisbon
Calçada do Moinho de Vento, 14B,
1150-236 Lisboa

coordenadas: 38.72045,-9.14131?z=19

Localização do MILL no mapa do [OpenStreetMap](https://osm.org/go/b5crq_xVM?layers=N):

[![Localização do MILL](https://centrolinux.pt/images/map.png "mapa com a localização do MILL, clique para mais detalhes e navegação")](https://osm.org/go/b5crq_xVM?layers=N)
